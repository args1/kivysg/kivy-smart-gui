# KivySG (Kivy Smart GUI)

This framework allow you to create simple crossplatform app: Windows, Linux, Android, iOS and Mac (note: iOS and Mac can't be tested as we don't have devices)

Based on [Kivy](https://kivy.org/) and [KivyMD](https://github.com/HeaTTheatR/KivyMD), the goal of KivySG is to create app as fast as possible, with a smooth GUI

To see how to create app with it you can look at [KivySG Sample](https://gitlab.com/args1/kivysg/kivy-smart-gui-sample)
