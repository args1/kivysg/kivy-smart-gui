# coding: utf-8

from kivy.app import App
from kivy.properties import ObjectProperty, StringProperty, NumericProperty

from kivymd.theming import ThemableBehavior
from kivymd.uix.list import OneLineIconListItem, MDList

from KivySG.container import SGContainer

import os


class NavbarItem(OneLineIconListItem):
    icon = StringProperty()
    sequence = NumericProperty()
    screen = ObjectProperty()


class NavbarList(ThemableBehavior, MDList):
    current_item = ObjectProperty()
    def set_color_item(self, instance_item):
        """Called when tap on a menu item."""
        if instance_item == self.current_item:
            return
        # Set the color of the icon and text for the menu item.
        for item in self.children:
            if item.text_color == self.theme_cls.primary_color:
                item.text_color = self.theme_cls.text_color
                break
        instance_item.text_color = self.theme_cls.primary_color
        self.current_item = instance_item
        instance_item.screen.set_screen()
        # self.parent.parent.parent.set_state()

    def add_item(self, text, screen, icon='', sequence=99):
        item = NavbarItem(text=text, screen=screen, icon=icon, sequence=sequence)
        self.add_widget(item)
        # self.children.sort(key=lambda x: x.sequence)


class Navbar(SGContainer):
    icon = StringProperty()
    subtitle = StringProperty()
    title = StringProperty()

    def add_menu_item(self, text, screen, icon='', sequence=99):
        self.ids.menu_list.add_item(text, screen, icon=icon, sequence=sequence)
