# coding: utf-8

from plyer import notification


def toast(text, duration=2):
    """
        Toast text for 2 second by default (duration)
    """
    notification.notify(message=text, toast=True, timeout=int(duration))
