# coding: utf-8

from kivy.app import App
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.screenmanager import Screen

from KivySG.container import SGContainer
from KivySG.navbar import Navbar

import os


class SGScreen(Screen):
    content_class = ObjectProperty(SGContainer)
    nav_drawer = ObjectProperty()
    nav_mode = StringProperty()
    title = StringProperty()
    icon = StringProperty()

    def __init__(self, *args, **kwargs):
        if not kwargs.get('name'):
            kwargs['name'] = kwargs.get('title').lower().replace(' ', '_')
        super().__init__(*args, **kwargs)
        app = self.get_app()
        app.add_screen(self)
        self.content.add_widget(self.content_class())

    def add_to_menu_item(self, sequence=99):
        nav = self.get_app().root_screen.nav_drawer.content_drawer
        return nav.add_menu_item(self.title, self,
                                 icon=self.icon, sequence=sequence)

    def get_app(self):
        return App.get_running_app()

    def init_nav_bar(self):
        app = self.get_app()
        conf = app.configs
        ConfApp = conf.get('App', dict())
        nav_mode = self.nav_mode or ConfApp['navigation']
        if nav_mode == 'navbar':
            navigation = Navbar(title=ConfApp['name'],
                                subtitle=ConfApp['subtitle'],
                                icon=ConfApp['icon'])
        else:
            raise ValueError(_('Unknown navigation mode: %s') % (nav_mode))
        self.nav_drawer = navigation
        self.add_widget(navigation)
        app.nav_drawer = navigation

    def set_screen(self):
        return self.get_app().set_screen(self)
