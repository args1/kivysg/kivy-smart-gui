# coding: utf-8

from kivy.app import App
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.screenmanager import Screen

from KivySG.container import SGContainer
from KivySG.navbar import Navbar

import os


class SGRootScreen(Screen):
    pass
