# coding: utf-8

import sys
import os

module_path = os.path.dirname(__file__)
if module_path not in sys.path:
    sys.path.append(module_path)

try:
    from main import SmartApp
except:
    from .main import SmartApp
