# coding: utf-8

from kivy.lang import Builder
from kivy.properties import DictProperty, ListProperty, ObjectProperty

from kivymd.app import MDApp

from KivySG.tools.translate import _
from KivySG.screen.root_screen import SGRootScreen

import configparser
import os

DEFAULT_CONFIG = [os.path.join(os.path.dirname(__file__), fpath) for fpath in [
    'config.ini',
]]

DEFAULT_VIEWS = [os.path.join(os.path.dirname(__file__), fpath) for fpath in [
    'container/container.kv',
    'navbar/navbar.kv',
    'screen/screen.kv',
    'screen/root_screen.kv',
    'screen/scrollable_screen.kv',
]]

def read_configs(config_path):
    config = dict()
    for path in config_path:
        fconf = configparser.ConfigParser()
        fconf.read(path)
        for section, values in fconf.items():
            if section not in config:
                config[section] = dict()
            for key, value in values.items():
                config[section][key] = value
    return config


class SmartApp(MDApp):
    config_files = ListProperty()
    configs = DictProperty()
    views = ListProperty()
    root = ObjectProperty()

    def build(self):
        self.config_files = DEFAULT_CONFIG + self.config_files
        self.views = DEFAULT_VIEWS + self.views
        self.configs = read_configs(self.config_files)

        self.load_views()
        self.root_screen = SGRootScreen()
        return self.root_screen

    def load_views(self):
        for views in self.views:
            ftype = views.split('.')[-1]
            if ftype == 'kv':
                Builder.load_file(views)
            else:
                pass

    def add_screen(self, screen):
        self.root_screen.screen_manager.add_widget(screen)

    def set_screen(self, screen):
        self.root_screen.screen_manager.current = screen.name

if __name__ == '__main__':
    SmartApp().run()
